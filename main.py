import numpy as np
import tensorflow as tf
import os.path
import data_reader

import matplotlib.pyplot as plt


np.random.seed(0)
tf.set_random_seed(0)

width = 20
height = 20
total_labels = 6


(dataset, labels) = data_reader.get_image(height, width, total_labels, 20)

key_input = raw_input("enter")
print ">>>", len(key_input)


def xavier_init(fan_in, fan_out, constant=0.1):
    """ Xavier initialization of network weights"""
    low = -constant * np.sqrt(6.0 / (fan_in + fan_out))
    high = constant * np.sqrt(6.0 / (fan_in + fan_out))
    return tf.random_uniform((fan_in, fan_out),
                             minval=low, maxval=high,
                             dtype=tf.float32)


class Classifier(object):

    def __init__(self, network_architecture, learning_rate=0.001, regularize=0.01, sparsity=0.01):
        self.network_architecture = network_architecture
        self.learning_rate = learning_rate
        self.regularize = regularize
        self.sparsity = sparsity
        print self.regularize, self.sparsity

        n_layers = len(network_architecture["n_features"])
        print network_architecture.viewkeys()
        self.x = tf.placeholder(tf.float32, [None, network_architecture["n_features"][0]])
        self.y = tf.placeholder(tf.float32, [None, network_architecture["n_features"][n_layers - 1]])
        self.z = tf.placeholder(tf.float32, [None, network_architecture["n_residues"][n_layers - 1]])

        self._create_network(n_layers)
        self._create_loss_optimizer()

        init = tf.initialize_all_variables()

        self.sess = tf.InteractiveSession()
        self.sess.run(init)

    def _create_network(self, n_layers):
        self.network_weights = self._initialize_weights(**self.network_architecture)

        KL_sparse = tf.convert_to_tensor(0.0, dtype=tf.float32)

        self.p = []
        self.r = []
        (p_, r_) = self._recognize0(
            self.x, self.network_weights['w'][0], self.network_weights['b'][0],
            self.network_weights['v'][0], self.network_weights['d'][0])
        KL_sparse = KL_sparse + self.sparsity_cost(r_)
        self.p.append(p_)
        self.r.append(r_)
        for i in xrange(1, n_layers - 1):
            (p_, r_) = self._recognize(
                self.p[i - 1], self.network_weights['w'][i], self.network_weights['b'][i],
                self.network_weights['v'][i],
                self.r[i - 1], self.network_weights['u'][i], self.network_weights['d'][i])
            KL_sparse = KL_sparse + self.sparsity_cost(r_)
            self.p.append(p_)
            self.r.append(r_)
        self.y_ = self.p[n_layers - 2]
        self.z = self.r[n_layers - 2]
        self.loss = tf.reduce_sum(-tf.add(tf.mul(self.y, tf.log(1e-10 + self.y_)), tf.mul(1 - self.y, tf.log(1e-10 + 1 - self.y_))))
        # self.loss = tf.reduce_sum(tf.square(self.y - self.y_))

        p_ = self.y
        for i in xrange(n_layers - 2, 0, -1):
            p_ = self._incomplete_generate(p_, self.network_weights['g'][i], self.network_weights['c'][i])
        p_ = self._incomplete_generate(p_, self.network_weights['g'][0], self.network_weights['c'][0])
        self.gen_incom = p_
        self.loss_incom = tf.reduce_sum(tf.square(self.gen_incom - self.x))

        p_ = self.y
        r_ = self.z
        for i in xrange(n_layers - 2, 0, -1):
            p_, r_ = self._generate(p_, self.network_weights['g'][i], self.network_weights['c'][i],
                                    self.network_weights['l'][i], self.network_weights['k'][i],
                                    r_, self.network_weights['h'][i], self.network_weights['e'][i])
        p_, r_ = self._generate(p_, self.network_weights['g'][0], self.network_weights['c'][0],
                                self.network_weights['l'][0], self.network_weights['k'][0],
                                r_, self.network_weights['h'][0], self.network_weights['e'][0])
        self.gen = p_
        self.loss_auc = tf.reduce_sum(tf.square(self.gen - self.x)) + self.regularize * KL_sparse

    def sparsity_cost(self, r):
        rate = tf.reduce_mean(r, 0)
        return tf.reduce_sum(tf.add(tf.mul(self.sparsity, tf.log(tf.div(self.sparsity, rate))), tf.mul(1 - self.sparsity, tf.log(tf.div(1 - self.sparsity, 1 - rate)))))

    def _initialize_weights(self, n_features, n_residues, num_unsupervised_layers):
        all_weights = dict()
        all_weights['w'] = []
        all_weights['b'] = []
        all_weights['g'] = []
        all_weights['c'] = []

        all_weights['u'] = []
        all_weights['d'] = []
        all_weights['h'] = []
        all_weights['e'] = []

        all_weights['v'] = []
        all_weights['k'] = []
        all_weights['l'] = []

        for i in xrange(0, len(n_features) - 1):
            prefix_ff = "ff/layer" + str(i)
            prefix_incom = "ffincom/layer" + str(i)
            prefix = "auc/layer" + str(i)
            all_weights['w'].append(tf.Variable(xavier_init(n_features[i], n_features[i + 1]), name=prefix_ff + "/w"))
            all_weights['b'].append(tf.Variable(tf.zeros([n_features[i + 1]], dtype=tf.float32), name=prefix_ff + "/b"))
            all_weights['g'].append(tf.Variable(xavier_init(n_features[i + 1], n_features[i]), name=prefix_incom + "/g"))
            all_weights['c'].append(tf.Variable(tf.zeros([n_features[i]], dtype=tf.float32), name=prefix_incom + "/c"))

            all_weights['u'].append(tf.Variable(xavier_init(n_residues[i], n_residues[i + 1]), name=prefix + "/u"))
            all_weights['d'].append(tf.Variable(tf.zeros([n_residues[i + 1]], dtype=tf.float32), name=prefix + "/d"))
            all_weights['h'].append(tf.Variable(xavier_init(n_residues[i + 1], n_residues[i]), name=prefix + "/h"))
            all_weights['e'].append(tf.Variable(tf.zeros([n_residues[i]], dtype=tf.float32), name=prefix + "/e"))

            all_weights['v'].append(tf.Variable(xavier_init(n_features[i], n_residues[i + 1]), name=prefix + "/v"))
            all_weights['k'].append(tf.Variable(xavier_init(n_features[i + 1], n_residues[i]), name=prefix + "/k"))
            all_weights['l'].append(tf.Variable(xavier_init(n_residues[i + 1], n_features[i]), name=prefix + "/l"))

        layer_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "auc")
        self.saver_auc = tf.train.Saver(var_list=layer_)

        layer_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "ffincom")
        self.saver_incom = tf.train.Saver(var_list=layer_)

        layer_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "ff")
        self.saver = tf.train.Saver(var_list=layer_)

        return all_weights

    def get_weights(self):
        w = self.sess.run((self.network_weights['w']), feed_dict={})
        b = self.sess.run((self.network_weights['b']), feed_dict={})
        g = self.sess.run((self.network_weights['g']), feed_dict={})
        c = self.sess.run((self.network_weights['b']), feed_dict={})
        u = self.sess.run((self.network_weights['u']), feed_dict={})
        d = self.sess.run((self.network_weights['d']), feed_dict={})
        h = self.sess.run((self.network_weights['h']), feed_dict={})
        e = self.sess.run((self.network_weights['e']), feed_dict={})
        v = self.sess.run((self.network_weights['v']), feed_dict={})
        k = self.sess.run((self.network_weights['k']), feed_dict={})
        l = self.sess.run((self.network_weights['l']), feed_dict={})
        return w, b, g, c, u, d, h, e, v, k, l

    def _incomplete_generate(self, p, g, c):
        p_ = tf.nn.sigmoid(tf.matmul(p, g, False))
        return p_

    def _generate(self, p, g, c, l, k, r, h, e):
        p_ = tf.nn.sigmoid(tf.add(tf.matmul(p, g, False), tf.matmul(r, l, False)))
        r_ = tf.nn.sigmoid(tf.add(tf.matmul(r, h, False), tf.matmul(p, k, False)))
        return p_, r_

    def _recognize0(self, p, w, b, v, d):
        p_ = tf.nn.sigmoid(tf.matmul(p, w, False))
        r_ = tf.nn.sigmoid(tf.matmul(p, v, False))
        return p_, r_

    def _recognize(self, p, w, b, v, r, u, d):
        p_ = tf.nn.sigmoid(tf.matmul(p, w, False))
        r_ = tf.nn.sigmoid(tf.add(tf.matmul(p, v, False), tf.matmul(r, u, False)))
        return p_, r_

    def _create_loss_optimizer(self):

        layer_auc = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "auc")
        self.optimizer_auc = tf.train.AdagradOptimizer(learning_rate=self.learning_rate).minimize(self.loss_auc, var_list=layer_auc)

        layer_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "ffincom")
        self.optimizer_incom = tf.train.AdagradOptimizer(learning_rate=self.learning_rate).minimize(self.loss_incom, var_list=layer_)

        layer_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "ff")
        self.optimizer = tf.train.AdagradOptimizer(learning_rate=self.learning_rate).minimize(self.loss, var_list=layer_)

    def fine_tune(self, X, Y):
        opt, loss = self.sess.run((self.optimizer, self.loss), feed_dict={self.y: Y, self.x: X})
        return loss

    def train_backward(self, X, Y):
        opt, loss = self.sess.run((self.optimizer_incom, self.loss_incom), feed_dict={self.y: Y, self.x: X})
        return loss

    def train(self, X, Y):
        opt, loss = self.sess.run((self.optimizer_auc, self.loss_auc), feed_dict={self.y: Y, self.x: X})
        return loss

    def transform(self, X):
        return self.sess.run((self.y_, self.z), feed_dict={self.x: X})

    def generate(self, Y, Z):
        return self.sess.run(self.gen, feed_dict={self.y: Y, self.z: Z})

    def generate_wo_residue(self, Y):
        return self.sess.run(self.gen_incom, feed_dict={self.y: Y})

    def terminate(self):
        self.sess.close()

    def save(self, file):
        self.saver.save(self.sess, file)

    def save_auc(self, file):
        self.saver_auc.save(self.sess, file)

    def save_incom(self, file):
        self.saver_incom.save(self.sess, file)

    def load(self, file):
        if os.path.isfile(file):
            self.saver.restore(self.sess, file)
            return True
        else:
            return False

    def load_auc(self, file):
        if os.path.isfile(file):
            self.saver_auc.restore(self.sess, file)
            return True
        else:
            return False

    def load_incom(self, file):
        if os.path.isfile(file):
            self.saver_incom.restore(self.sess, file)
            return True
        else:
            return False


def train(network_architecture, batch_size=50, training_epochs=[10, 10, 10], display_step=100):
    network = Classifier(network_architecture, learning_rate=0.1, regularize=1000, sparsity=0.05)

    shuff = range(dataset.shape[0])
    np.random.shuffle(shuff)
    shuff_data = dataset[shuff, ...]
    shuff_labels = labels[shuff]

    if not network.load("./weights/ff.weights") or (len(key_input) >= 1 and key_input[0] == 't'):
        print "training ff ..."
        prev_loss = 0.
        for epoch in range(training_epochs[0]):
            avg_loss = 0.
            total_batch = dataset.shape[0] / batch_size
            for i in range(total_batch):
                batch_xs = shuff_data[(i * batch_size): (i + 1) * batch_size, ...].reshape((-1, width * height))
                batch_ys = shuff_labels[(i * batch_size): (i + 1) * batch_size]
                loss = network.fine_tune(batch_xs, batch_ys)
                avg_loss += loss / total_batch

            if epoch % display_step == 0:
                print "Epoch:", '%04d' % (epoch + 1), "loss=", "{:.9f}".format(avg_loss)

            if abs(avg_loss - prev_loss) < 1e-8:
                break

            prev_loss = avg_loss

        network.save("./weights/ff.weights")

    if not network.load_incom("./weights/ffincom.weights") or (len(key_input) >= 2 and key_input[1] == 't'):
        print "training ff backward ..."
        prev_loss = 0.
        for epoch in range(training_epochs[1]):
            avg_loss = 0.
            total_batch = dataset.shape[0] / batch_size
            for i in range(total_batch):
                batch_xs = shuff_data[(i * batch_size): (i + 1) * batch_size, ...].reshape((-1, width * height))
                batch_ys = shuff_labels[(i * batch_size): (i + 1) * batch_size]
                loss = network.train_backward(batch_xs, batch_ys)
                avg_loss += loss / total_batch

            if epoch % display_step == 0:
                print "Epoch:", '%04d' % (epoch + 1), "loss=", "{:.9f}".format(avg_loss)

            if abs(avg_loss - prev_loss) < 1e-8:
                break

            prev_loss = avg_loss

        network.save_incom("./weights/ffincom.weights")

    if not network.load_auc("./weights/auc.weights") or (len(key_input) >= 3 and key_input[2] == 't'):
        print "training auc ..."
        prev_loss = 0.
        for epoch in range(training_epochs[2]):
            avg_loss = 0.
            total_batch = dataset.shape[0] / batch_size
            for i in range(total_batch):
                batch_xs = shuff_data[(i * batch_size): (i + 1) * batch_size, ...].reshape((-1, width * height))
                batch_ys = shuff_labels[(i * batch_size): (i + 1) * batch_size]
                loss = network.train(batch_xs, batch_ys)
                avg_loss += loss / total_batch

            if epoch % display_step == 0:
                print "Epoch:", '%04d' % (epoch + 1), "loss=", "{:.9f}".format(avg_loss)

            if abs(avg_loss - prev_loss) < 1e-8:
                break

            prev_loss = avg_loss

        network.save_auc("./weights/auc.weights")

    return network

network_architecture = dict(n_features=[width * height, 100, total_labels], n_residues=[1, 100, 100], num_unsupervised_layers=2)

classifier = train(network_architecture, training_epochs=[5000, 2000, 10000])

(out, z) = classifier.transform(dataset.reshape((-1, width * height)))

(w, b, g, c, u, d, h, e, v, k, l) = classifier.get_weights()

ny = 10
nx = 10
canvas = np.empty((height * ny, width * nx))
for y in xrange(0, ny, 1):
    for x in xrange(0, nx, 1):
        li = (y * nx + x) * v[0].shape[1] / (ny * nx)
        gen = v[0][:, li]
        canvas[y * height:(y + 1) * height, x * width:(x + 1) * width] = gen.reshape((height, width))

plt.figure(figsize=(8, 10))
plt.imshow(canvas, origin="upper", cmap='Greys_r')
plt.tight_layout()
plt.show()

ny = 1
nx = 5
canvas = np.empty((height * ny, width * nx))
for y in xrange(0, ny, 1):
    for x in xrange(0, nx, 1):
        li = x * out.shape[0] / nx
        canvas[y * height:(y + 1) * height, x * width:(x + 1) * width] = dataset[li, ...][:, :, 0]

plt.figure(figsize=(8, 10))
plt.imshow(canvas, origin="upper", cmap='Greys_r')
plt.tight_layout()
plt.show()

ny = 1
nx = 5
canvas = np.empty((height * ny, width * nx))
for y in xrange(0, ny, 1):
    for x in xrange(0, nx, 1):
        li = x * out.shape[0] / nx
        gen = classifier.generate_wo_residue(out[li:(li + 1)])
        canvas[y * height:(y + 1) * height, x * width:(x + 1) * width] = gen.reshape((height, width))

plt.figure(figsize=(8, 10))
plt.imshow(canvas, origin="upper", cmap='Greys_r')
plt.tight_layout()
plt.show()

ny = 10
nx = 40
canvas = np.empty((height * ny, width * nx))
for y in xrange(0, ny, 1):
    for x in xrange(0, nx, 1):
        li = y * out.shape[0] / ny
        zi = x * out.shape[0] / nx
        gen = classifier.generate(labels[li:(li + 1)], z[zi:(zi + 1)])
        canvas[y * height:(y + 1) * height, x * width:(x + 1) * width] = gen.reshape((height, width))

plt.figure(figsize=(8, 10))
plt.imshow(canvas, origin="upper", cmap='Greys_r')
plt.tight_layout()
plt.show()

classifier.terminate()
