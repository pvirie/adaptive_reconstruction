import numpy as np
import tensorflow as tf
import os.path
import data_reader

import matplotlib.pyplot as plt

from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)


np.random.seed(0)
tf.set_random_seed(0)

width = 28
height = 28
total_labels = 6


(dataset, labels) = data_reader.get_image(height, width, total_labels, 20)

key_input = raw_input("enter")
print ">>>", len(key_input)


def xavier_init(fan_in, fan_out, constant=0.1):
    """ Xavier initialization of network weights"""
    low = -constant * np.sqrt(6.0 / (fan_in + fan_out))
    high = constant * np.sqrt(6.0 / (fan_in + fan_out))
    return tf.random_uniform((fan_in, fan_out),
                             minval=low, maxval=high,
                             dtype=tf.float32)


class AUC(object):

    def __init__(self, network_architecture, learning_rate=0.001, regularize=0.01, sparsity=0.01, weight_decay=0.01):
        self.network_architecture = network_architecture
        self.learning_rate = learning_rate
        self.regularize = regularize
        self.sparsity = sparsity
        self.weight_decay = weight_decay

        n_layers = len(network_architecture["n_features"])
        print network_architecture.viewkeys()
        self.x = tf.placeholder(tf.float32, [None, network_architecture["n_features"][0]])
        self.y = tf.placeholder(tf.float32, [None, network_architecture["n_features"][n_layers - 1]])

        self._create_network(n_layers)
        self._create_loss_optimizer()

        init = tf.initialize_all_variables()

        self.sess = tf.InteractiveSession()
        self.sess.run(init)

    def _create_network(self, n_layers):
        self.network_weights = self._initialize_weights(**self.network_architecture)

        KL_sparse = tf.convert_to_tensor(0.0, dtype=tf.float32)
        decay_cost = tf.convert_to_tensor(0.0, dtype=tf.float32)

        self.p = []
        p_ = self._recognize(self.x, self.network_weights['w'][0], self.network_weights['b'][0])
        KL_sparse = KL_sparse + self.sparsity_cost(p_)
        decay_cost = decay_cost + tf.reduce_mean(tf.square(self.network_weights['w'][0]))
        self.p.append(p_)
        for i in xrange(1, n_layers - 1):
            p_ = self._recognize(self.p[i - 1], self.network_weights['w'][i], self.network_weights['b'][i])
            KL_sparse = KL_sparse + self.sparsity_cost(p_)
            decay_cost = decay_cost + tf.reduce_mean(tf.square(self.network_weights['w'][i]))
            self.p.append(p_)
        self.y = self.p[n_layers - 2]

        p_ = self.y
        for i in xrange(n_layers - 2, 0, -1):
            p_ = self._generate(p_, self.network_weights['g'][i], self.network_weights['c'][i])
            decay_cost = decay_cost + tf.reduce_mean(tf.square(self.network_weights['g'][i]))
        p_ = self._generate(p_, self.network_weights['g'][0], self.network_weights['c'][0])
        decay_cost = decay_cost + tf.reduce_mean(tf.square(self.network_weights['g'][0]))
        self.gen = p_
        self.loss = tf.reduce_mean(tf.square(self.gen - self.x)) + self.regularize * KL_sparse + self.weight_decay * decay_cost

    def sparsity_cost(self, r):
        rate = tf.reduce_mean(r, 0)
        return tf.reduce_mean(tf.add(tf.mul(self.sparsity, tf.log(tf.div(self.sparsity, rate))), tf.mul(1 - self.sparsity, tf.log(tf.div(1 - self.sparsity, 1 - rate)))))

    def _initialize_weights(self, n_features):
        all_weights = dict()
        all_weights['w'] = []
        all_weights['b'] = []
        all_weights['g'] = []
        all_weights['c'] = []

        for i in xrange(0, len(n_features) - 1):
            prefix = "auc/layer" + str(i)
            all_weights['w'].append(tf.Variable(xavier_init(n_features[i], n_features[i + 1]), name=prefix + "/w"))
            all_weights['b'].append(tf.Variable(tf.zeros([n_features[i + 1]], dtype=tf.float32), name=prefix + "/b"))
            all_weights['g'].append(tf.Variable(xavier_init(n_features[i + 1], n_features[i]), name=prefix + "/g"))
            all_weights['c'].append(tf.Variable(tf.zeros([n_features[i]], dtype=tf.float32), name=prefix + "/c"))

        layer_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "auc")
        self.saver = tf.train.Saver(var_list=layer_)

        return all_weights

    def get_weights(self):
        w = self.sess.run((self.network_weights['w']), feed_dict={})
        b = self.sess.run((self.network_weights['b']), feed_dict={})
        g = self.sess.run((self.network_weights['g']), feed_dict={})
        c = self.sess.run((self.network_weights['b']), feed_dict={})
        return w, b, g, c

    def _generate(self, p, g, c):
        p_ = tf.nn.sigmoid(tf.add(tf.matmul(p, g, False), c))
        return p_

    def _recognize(self, p, w, b):
        p_ = tf.nn.sigmoid(tf.add(tf.matmul(p, w, False), b))
        return p_

    def _create_loss_optimizer(self):

        layer_ = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "auc")
        self.optimizer = tf.train.AdagradOptimizer(learning_rate=self.learning_rate).minimize(self.loss, var_list=layer_)

    def train(self, X):
        opt, loss = self.sess.run((self.optimizer, self.loss), feed_dict={self.x: X})
        return loss

    def transform(self, X):
        return self.sess.run(self.y, feed_dict={self.x: X})

    def generate(self, Y):
        return self.sess.run(self.gen, feed_dict={self.y: Y})

    def terminate(self):
        self.sess.close()

    def save(self, file):
        self.saver.save(self.sess, file)

    def load(self, file):
        if os.path.isfile(file):
            self.saver.restore(self.sess, file)
            return True
        else:
            return False


def train(network_architecture, batch_size=100, training_epochs=[10, 10, 10], display_step=5):
    # more weight_decay = blur
    # more regularize = sharper feature edges
    network = AUC(network_architecture, learning_rate=0.1, regularize=0.01, sparsity=0.05, weight_decay=0.0001)

    # shuff = range(dataset.shape[0])
    # np.random.shuffle(shuff)
    # shuff_data = dataset[shuff, ...]

    if not network.load("./weights/sparse_auc.weights") or (len(key_input) >= 1 and key_input[0] == 't'):
        print "training auc ..."
        prev_loss = 0.
        for epoch in range(training_epochs[0]):
            avg_loss = 0.
            total_batch = mnist.train.num_examples / batch_size
            for i in range(total_batch):
                # batch_xs = shuff_data[(i * batch_size): (i + 1) * batch_size, ...].reshape((-1, width * height))
                batch_xs, batch_ys = mnist.train.next_batch(batch_size)
                loss = network.train(batch_xs)
                avg_loss += loss / total_batch

            if epoch % display_step == 0:
                print "Epoch:", '%04d' % (epoch + 1), "loss=", "{:.9f}".format(avg_loss)

            if abs(avg_loss - prev_loss) < 1e-8:
                break

            prev_loss = avg_loss

        network.save("./weights/sparse_auc.weights")

    return network

network_architecture = dict(n_features=[width * height, 100])

auc = train(network_architecture, training_epochs=[200])

# out = auc.transform(dataset.reshape((-1, width * height)))
batch_xs, batch_ys = mnist.train.next_batch(100)
out = auc.transform(batch_xs)

(w, b, g, c) = auc.get_weights()

ny = 10
nx = 10
canvas = np.empty((height * ny, width * nx))
for y in xrange(0, ny, 1):
    for x in xrange(0, nx, 1):
        li = (y * nx + x) * w[0].shape[1] / (ny * nx)
        gen = w[0][:, li]
        canvas[y * height:(y + 1) * height, x * width:(x + 1) * width] = gen.reshape((height, width))

plt.figure(figsize=(8, 10))
plt.imshow(canvas, origin="upper", cmap='Greys_r')
plt.tight_layout()
plt.show()

ny = 20
nx = 20
canvas = np.empty((height * ny, width * nx))
for y in xrange(0, ny, 1):
    for x in xrange(0, nx, 1):
        li = (y * nx + x) * out.shape[0] / (ny * nx)
        gen = auc.generate(out[li:(li + 1)])
        canvas[y * height:(y + 1) * height, x * width:(x + 1) * width] = gen.reshape((height, width))

plt.figure(figsize=(8, 10))
plt.imshow(canvas, origin="upper", cmap='Greys_r')
plt.tight_layout()
plt.show()

auc.terminate()
