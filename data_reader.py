import numpy as np
import os.path
from PIL import Image

prefix = "./data"
wprefix = "./export"


def write(w, b):
    f = open(wprefix + '/master.txt', 'w')
    f.write(str(len(w)) + '\n')
    for i in xrange(0, len(w)):
        s = '/w' + str(i) + '.data'
        np.save(wprefix + s, w[i])
        f.write(s + '\n')
    f.write(str(len(b)) + '\n')
    for i in xrange(0, len(b)):
        s = '/b' + str(i) + '.data'
        np.save(wprefix + s, b[i])
        f.write(s + '\n')
    f.close()


def rgb2gray(rgb):
    return np.expand_dims(np.dot(rgb[..., :3], [0.299, 0.587, 0.114]), axis=2)


def get_image(height, width, total_labels, max_sample_per_label):
    data = []
    labels = []
    for i in range(0, total_labels):
        for j in range(0, max_sample_per_label):
            file_name = prefix + '/' + str(i) + ' (' + str(j) + ').png'
            if os.path.isfile(file_name):
                img = Image.open(file_name)
                img = img.resize((width, height), Image.ANTIALIAS)
                processed = np.asarray(img) / 255.0

                data.append(rgb2gray(processed))

                label = np.zeros((total_labels), dtype=float)
                label[i] = 1
                labels.append(label)

                # plt.figure(1)
                # plt.imshow(processed)
                # plt.ion()
                # plt.show()
                # key_input = raw_input("enter")
                # print ">>>", i, len(key_input)

    d = np.array(data)
    l = np.array(labels)
    return d, l
